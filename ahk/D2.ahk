#SingleInstance force
#Persistent
#NoEnv

#IfWinActive, Diablo II

	$Space::
	{
		While GetKeyState("Space", "P")
		{
			if !GetKeyState("LButton", "P")
			{
				SendInput {LButton Down}
			}
			Sleep 100
		}
		SendInput {LButton Up}
	}
	Return

	~A::
	{
		Sleep 100
		SendInput {RButton}
	}
	Return

	~S::
	{
		Sleep 100
		SendInput {RButton}
	}
	Return

	~D::
	{
		Sleep 100
		SendInput {RButton}
	}
	Return

	~F::
	{
		Sleep 100
		SendInput {RButton}
	}
	Return

#IfWinActive
